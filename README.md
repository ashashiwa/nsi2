# The Naval Strike Initiative

***

#### Description
The Naval Strike Initiative est un ***jeu de bataille navale en réseau***.
Il a été réalisé par Médérick LG. et Simon RE. en terminale NSI du lycée Lalande à Bourg-en-Bresse (01) pour participer à l'évènement [Trophées NSI 2024](https://trophees-nsi.fr/).


#### Configuration
A partir de Python 3.6
Focntionne sous Windows 10 et Ubuntu 18.04 (Linux)


#### Installation
Que vous soyez sur Linux ou Windows, il vous faut installer le module 'pygame'.
Pour cela, réaliser la commande suivante dans votre intérpréteur Python : 'pip install pygame'


#### Démarrage du jeu
1. Chaque joueur ouvre un intérpréteur Python (Spyder, PyCharm, ...)
2. Un des deux joueurs doit lancer le script 'serveur.py' pour que les deux ordinateurs puissent communiquer
3. Les deux joueurs lancent le script 'The Naval Strike Initiative.py'
4. Dans 'SERVEUR' et dans 'PORT', il faut rentrer manuellement les valeurs indiquées dans la console Python de 'serveur.py'
5. Une fois que les deux joueurs ont cliqué sur 'OK', le jeu peut commencer


#### Précisions
Pour plus d'informations pratiques, reportez vous au fichier 'requirements.txt' ci joint.