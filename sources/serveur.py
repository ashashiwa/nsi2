import socket
from _thread import *
import sys

from random import randint

def test():
    """permet de découvrir un port qui fonctionne.
    ATTENTION : sous Linux, cette fonctionnalité est bizarrement aléatoire"""
    if sys.platform == "linux":
        var = 111
    if "win" in sys.platform :
        var = 10049
    for port in range(10300,10400):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex( ('0.0.0.0', port))
        
        if result == var:
            return socket.gethostname() , port
            break

server = "0.0.0.0"
pc_name, port = test()

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    s.bind((server, port))
except socket.error as e:
    str(e)

print(f"En attent de clients sur \nserveur: {pc_name}\nport :  {port}")
tour=0                  #Compteur de joueurs
numero = randint(0,1)   #tirage au sort de celui qui commence
ordis_connectes = {}    #dictionnaire des addresses Adresse->numero et numero->addresse
dico_donnee = {}        #permet la discussion entre les clients lors du "polling"

while True:
    message, addr = s.recvfrom(128);                                #ecoute
    if message.decode()=="coucou" :                                 #première connexion d'un client
        print("Un nouveau client : ", addr, "numéro :", numero)
        ordis_connectes[addr]=numero
        ordis_connectes[numero]=addr

        s.sendto(str(numero).encode(), addr)                        #envoi du numéro du client

        if numero==1 : numero=0
        else : 
           numero =1 
        
        
    else :                                                          #les présentations sont faites. On joue maintenant
            """les client demandent ce qu'ils veulent. Soit une case, si on est attaqué, soit un entier, si on vient d'attaquer.
            Ils demandent donc "case ?" ou "entier ?"
            Le serveur gère ces demandes, remplis un dictionnaire quand les données arrivent et les transmet au client"""

            if "?" in message.decode() :                        #c'est une demande:
                if str(message.decode())=='case ?':
                    if dico_donnee.get('case') is not None :                    #une case a été fournie
                        s.sendto(str(dico_donnee.get('case')).encode(), addr)   #on envoie, on efface
                        dico_donnee['case']=None
                    else : 
                        s.sendto(str(0).encode(), addr)

                if str(message.decode())=='entier ?':
                    if dico_donnee.get('entier') is not None :                  #un entier a été fourni
                        message = f"('reponse', {dico_donnee.get('entier')})"   #on envoie et on efface tout : le tour de jeu est fini.
                        s.sendto(message.encode() , addr)
                        dico_donnee = {}
                    else : 
                        s.sendto(str(0).encode(), addr)
            elif "!" in message.decode() :                            #c'est la fin... on ferme le serveur.
                s.sendto("Bonne journée".encode(), addr)
                dico_donnee['entier']="'GAGNE'"

            else : #ce sont des données
                liste_message= eval(message.decode()) #on reçoit un tuple : exemple : ('entier', 5) ou ('case', ('A',5))
                dico_donnee[liste_message[0]] = liste_message[1]
                s.sendto("bien reçu".encode() , addr)
            tour +=1
