
# This file is part of the XXX distribution (https://github.com/xxxx or http://xxx.github.io).
# Copyright (c) 2015 Liviu Ionescu.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import pygame
from random import choice, randint
from PIL import Image
import socket
import numpy as np  

#####CLASS NAVIRE#####

class Navire():
    def __init__(self, longueur, coordonnees):
        assert type(longueur) == int
        assert type(coordonnees) == type(list())
        assert len(coordonnees)-1 == longueur
        self.longueur = longueur
        self.positions = coordonnees
    
    def estTouche(self,coordonnee):
        """
        
        PARAMETRES :
            -> coordonnee = tuple
            
        ACTION :
            Si les coordonnées rentrées en paramètres appartiennent à celles d'un bateau (touché), estTouche renvoie 1
            Si les coordonnées rentrées en paramètres n'appartiennet pas à celles d'un bateau (dans l'eau'), estTouché renvoie 0
            Si les coordonnées rentrées en paramètres appartiennent à celles d'un bateau et qu'il s'agit du dernier bour de bateau (touché coulé), estTouché renvoie 2
                    
        """
        if coordonnee in self.positions:
            self.positions.remove(coordonnee)
            if len(self.positions) == 1:
                return 2
            else:
                return 1
        else:
            return 0
        
#####

#####CLASS GRILLE#####

class Grille():
    def __init__(self):
        self.grille = [['X' for _ in range(10)] for _ in range(10)]
        self.navires = []
        self.zone_interdite = []
        self.longueurs = [2,3,3,4,5]
        #self.longueurs = [2] #,3,3,4,5]
             
    def remplir(self):
        """
        
        PARAMETRES :
            aucun
            
        ACTION :
            Remplit la grille et les positions des navires en fonction de :
                - self.longueurs (longueurs des bateaux)
                - self.zone_interdite (zone autour des bateaux pour ne pas que les bateaux se superposent et/ou se touchent)
            
        """
        lettre_chiffre = {chr(65+i):i for i in range(0,10) }
        directions = ['horiz', 'vert']
        on_place = False
        cptr= 0
        while self.longueurs!=[]:
            while not on_place : 
                cptr+=1
                #on place :
                on_place = True
                longueur = choice(self.longueurs)
                direction = choice(directions)
                if direction =='horiz' : 
                    
                    position = \
                ( choice([chr(i) for i in range(65,65+10)]), \
                randint(1,10-longueur) )
                else : 
                    position = \
                ( choice([chr(i) for i in range(65,65+10-longueur)]), \
                randint(1,10) )
                #verification si cela sort du cadre
                
                if direction =='vert':
                    if 0 <= longueur +lettre_chiffre[position[0]]  >=10 : 
                        on_place=False
                        break
                else : 
                    if 0 <= longueur + position[1] >=10 : 
                        on_place=False
                        break                
               #verification si les cases sont libres
               #pour chaque cases, verifier si ça vaut X.
                #Sinon, on_place à False
                if direction=='vert' :
                    for i in range(longueur):
                        print(position)
                        if self.grille[lettre_chiffre[position[0]]-1][position[1]-1]!='X' :
                            on_place=False
                        break
                    positions_navire = [(chr(lettre_chiffre[position[0]]+i+65), position[1]) for i in range(longueur)]
                    positions_navire.append("v")
                else : 
                    for i in range(longueur):
                        
                        if self.grille[lettre_chiffre[position[0]]-1][position[1]+i]!='X' :
                            on_place=False
                            break
                    positions_navire = [(chr(lettre_chiffre[position[0]]+65), position[1]+i) for i in range(longueur)]
                    positions_navire.append("h")
                try :
                    for case in positions_navire:
                        for position_interdite in self.zone_interdite:
                            if case == position_interdite:
                                on_place = False
                                break
                except      :
                    pass
            
            if on_place==True :
                #on enlève la longueur choisie
                self.longueurs.remove(longueur)
                
                self.remplir_zone_interdite(direction,position,longueur)

                #on cree un navire avec les bonnes coordonnees et on l'ajoute à la liste navire
                self.navires.append(Navire(longueur,positions_navire))
                #on met à 'O' les positions du navire
                for lettre, chiffre in positions_navire[:-1]:
                    self.grille[lettre_chiffre[lettre]][chiffre-1]='O'
                #print(position, positions_navire,self.zone_interdite)
                print("positions_navire", positions_navire)
                on_place = False

    def afficher(self):
        """
        
        PARAMETRES :
            aucun
            
        ACTION :
            Affiche la grille dans la console Python
        
        """
        grille = "|\t1\t2\t3\t4\t5\t6\t7\t8\t9\t10|"
        grille += "\t___________________________________________________________________________________\n"
        for i in range(10):
            texte_ligne = f"{chr(65+i)}|"
            for carac in self.grille[i]:
                texte_ligne+= "\t"
                texte_ligne+=carac
            texte_ligne += "\n"
            grille += texte_ligne
        print(grille)

    def remplir_zone_interdite(self,direction,position,longueur):
        """
        
        PARAMETRES :
            -> direction (du bateau)
            -> position (de chaque morceaux du bateau)
            -> longueur (du bateau)
            
        ACTION :
            Remplit la variable self.zone_interdite pour qu'aucun bateau ne se chevauche
        
        """
        positions=[]
        for j in range(-1,longueur+2):
            x,y = position
            for i in range(-1,2):
                if direction == 'vert':           
                    x1 = ord(x)
                    lettre = chr(x1+j)
                    chiffre = y+i
                    if lettre in [chr(i) for i in range(65,75)] and 1<=chiffre<=10:
                        positions.append((lettre,chiffre))
                if direction == 'horiz':
                    x1 = ord(x)
                    lettre = chr(x1+i)
                    chiffre = y+j
                    if lettre in [chr(i) for i in range(65,75)] and 1<=chiffre<=10:
                        positions.append((lettre,chiffre))
        self.zone_interdite+=positions

    def attaquer(self,coordonnees):
        """
        
        PARAMETRES :
            coordonnees (de là où on décide d'attaquer)
                         
        ACTION :
            Fait le lien entre la Class Navire et la Class Grille pour savoir si la position correspond à touché, touché coulé ou dans l'eau
            Renvoie 0, 1 ou 2 suivant si la position tombe dans l'eau, sur un bout de bateau ou sur le bateau en entier

        """
        if self.navires == []:
            return 3
        resultat = 0
        for bateau in self.navires:
            resultat_chiffre= bateau.estTouche(coordonnees)
            #print('ok',bateau.estTouche(coordonnees) )
            if resultat_chiffre == 1:
                resultat = 1
                break
            if resultat_chiffre == 2:
                resultat = 2
                self.navires.remove(bateau)
                break
        return resultat   
        
    def afficher_positions_navires(self):
        """
        
        PARAMETRES :
            aucun
        
        ACTION :
            Affiche les positions des navires dans la console Python
        
        """
        for bateau in self.navires:
            pass

#####

#####CLASS APP#####

class App:
    def __init__(self):
        self._running = True
        self._display_surf = None
        self.size = self.weight, self.height = 1200, 1000
        self.explosion_liste = []
        self.bateaux_liste = []
        self.cercles = []
        self.tick_depart = pygame.time.get_ticks()
        
        self.clock_move = 0
        self.clock_frame = 0
        
        self.grille = Grille()
        self.grille.remplir()
        self.grille.afficher()
        self.grille.afficher_positions_navires()        
        self.reponse = 'Touche'
        self.cercles = []
        self.cMonTour = None
        self.serveur = None
        self.socket_medsi = ""
        
        self.l_case_cliquee = []
        
        self.image_grille = self.load_from_pyxel_image(file_ = 'tilemap0.txt', offset_x = 0, offset_y=0, L=768, H = 256)
        self.image_tortue  = self.load_from_pyxel_image(file_ = 'image0.txt')
        self.image_horiz_gauche = self.load_from_pyxel_image(file_ = 'image0.txt', offset_x = 24, offset_y=8)
        self.image_horiz_droite = self.load_from_pyxel_image(file_ = 'image0.txt', offset_x = 24, offset_y=104)
        self.image_horiz_milieu = self.load_from_pyxel_image(file_ = 'image0.txt', offset_x = 24, offset_y=152)
        self.image_vert_haut = self.load_from_pyxel_image(file_ = 'image0.txt', offset_x = 72, offset_y=8)
        self.image_vert_bas = self.load_from_pyxel_image(file_ = 'image0.txt', offset_x = 72, offset_y=104)
        self.image_vert_milieu = self.load_from_pyxel_image(file_ = 'image0.txt', offset_x = 72, offset_y=56)
        
        self.position_cliquee_x = 0
        self.position_cliquee_y = 0
        
        self.etat_interface=1 #1 écran de début #2 jeu #3 écran de fin
        
        #rectangles réseau
        self.button1_rect = pygame.Rect(500, 600, 132, 21)
        self.button2_rect = pygame.Rect(500,680,137,21)
        self.connected=False
        
        # initialisation des boîtes de saisies (couleur,taille,emplacement,texte etc...)
        screen_width, screen_height = self.weight, self.height
         
        self.input_box = pygame.Rect(screen_width / 2 - 100, screen_height / 2 - 32, 140, 32)  
        self.input_box2 = pygame.Rect(screen_width / 2 - 100, screen_height / 2 + 100, 140, 32)  
        self.button_ok_connect = pygame.Rect(800, 700, 67, 37)
        self.text_IP = 'localhost' 
        self.text_PORT = ''
        self.pc_name = ""
        self.port=""
        self.text_Error = ""
        self.active_port=True
        self.active_ip=False
        
        self.gauche_liste_cases_touchees = []
        self.gauche_liste_cases_eau = []
        
        self.droite_liste_cases_touchees = []
        self.droite_liste_cases_eau = []
        
        self.case_cliquee = None   
        self.on_attend_entier = False
        

   
    
    def load_from_pyxel_image(self, file_ = 'image0.txt', offset_x = 24, offset_y=56, L=38, H=38, debug=0):
        """
        
        PARAMETRES :
            -> file_ = image0.txt
            -> offset_x =
            -> offset_y =
            -> L = largeur des 
            -> H = hauteur
            -> debug =
        ACTION :
            Nos images des bateaux ont été réalisées sur Pyxel, ne voulant pas refaire les images sur Pygame, nous avons crée un programme qui modifie des bits du fichier image0.txt (fichier de Pyxel) en couleurs
            Retourne une image
            
        """
        assert isinstance(file_, str)
        assert isinstance(offset_x, int)
        assert isinstance(offset_y, int)
        assert isinstance(L, int)
        assert isinstance(H, int)
        
        dico = {'0': (0,0,0), '1' :(43, 51, 95), '2':(126, 32, 114), '3':( 25, 149, 156), '4': (139, 72, 82), '5' :(57, 92, 152), '6' :(169, 193, 255),'7':(238,238,238),'8' :(212, 24, 108),'9' :(211, 132, 65),'a' :(233, 295, 91), 'b' :(112, 198, 169), 'c' :(118, 150, 222), 'd' :(163, 163, 163), 'e' :(255, 251, 152), 'f' :(237, 199, 176)}
        R,G,B = [], [], []
        with open(file_) as fichier : 
            for _ in range (offset_y) : 
                fichier.readline()
            
            for _ in range(H) : 
                ligne = []
                
                ligne = fichier.readline()[offset_x:offset_x+L]
                for carac in ligne :
                    #print(ord(carac))
                    try :
                        R.append(dico.get(str(carac))[0])
                        G.append(dico.get(str(carac))[1])
                        B.append(dico.get(str(carac))[2])
                    except :
                        print(ord(carac))
                    #contenu_image.append(dico.get(str(carac)))
                #fichier.readline()
        R = np.array(R).astype('int8')    
        G = np.array(G).astype('int8')         
        B = np.array(B).astype('int8')  
        
        R.shape= (L,H)
        G.shape= (L,H)
        B.shape= (L,H)
        
        arr = np.dstack([R, G, B])
        img = Image.fromarray(arr, 'RGB')
        if debug : 
            img.save("imagetransformee.png")
            img.show()
        return img
    
    def pilImageToSurface(self, pilImage):
        """
        
        PARAMETRES :
            -> pilImage = image résultant de la méthode précédente (load_from_pyxel_image)
            
        ACTION :
            Retourne une surface image lisible par Pygame

        """
        return pygame.image.fromstring(pilImage.tobytes(), pilImage.size, pilImage.mode).convert()      
                 
    def renvoie_case(self, position_cliquee_x, position_cliquee_y):
        """
            
            PARAMETRES :
                -> position_cliquee_x = int
                -> position_cliquee_y = int
            ACTION :
                Renvoie la case (sous forme de tuple en fonction des coordonnées de la souris
                Renvoie (None,None) si les coordonees cliquees ne correspondent pas à une case (hors de la grille)
                
        """
        a_renvoyer = ((chr(65+(position_cliquee_y-260)//48)), (1+(position_cliquee_x-(80))//48)) if 80 < position_cliquee_x < 80+480 and 260 < position_cliquee_y < 260+480 else (None,None)
        if a_renvoyer != (None,None):
           self.l_case_cliquee.append(a_renvoyer)
        return a_renvoyer
    
    def on_init(self):
        """
        
        PARAMETRES :
            aucun

        ACTION :
            Initialise l'écran Pygame

        """
        pygame.init()
        self.font = pygame.font.SysFont("Eight-Bit Madness", 40)
        self.clock = pygame.time.Clock()
        self.clock.tick(10)
        self._display_surf = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running = True

 
    def on_event(self, event):
        """
        
        PARAMETRES :
            -> event = l'évènement
        
        ACTION:
            Gère les actions en fonction de l'évènement
            Ici, cela modifie self.etat_interface quand les deux ordinateurs sont connectes par exemple
            Et cela gère les clics et les cases cliquées
        
        """
        if event.type == pygame.QUIT:
            self._running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # Clic gauche
                if self.etat_interface==2 :
                    if len(self.l_case_cliquee) != 0:
                        self.l_case_cliquee.pop(0)
                    self.position_cliquee_x, self.position_cliquee_y = pygame.mouse.get_pos()
                    self.case_cliquee=self.renvoie_case(self.position_cliquee_x,self.position_cliquee_y)
                    self.l_case_cliquee.append(self.case_cliquee)
                    
                
        if self.etat_interface==1 : #fenetre connexion client
            if event.type == pygame.MOUSEBUTTONDOWN:
                # Vérifie si la première boîte de saisie est cliquée
                if self.input_box.collidepoint(event.pos):
                    self.active_port = False
                    self.active_ip = True
                # Vérifie si la deuxième boîte de saisie est cliquée
                elif self.input_box2.collidepoint(event.pos):
                    self.active_ip = False
                    self.active_port = True
                elif self.button_ok_connect.collidepoint(event.pos):
                    ##Lance la connexion
                    #try:
                        print(f"connexion sur {self.text_IP}:{int(self.text_PORT)}")
                        #self.connexion = Network(self.text_IP, int(self.text_PORT))
                        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM); 
                        
                        self.socket.sendto("coucou".encode(), (self.text_IP, int(self.text_PORT)));
                        reponse = self.socket.recv(1024);
                        
                        print('longueur', len(reponse), reponse)
                        self.cMonTour= bool(eval(reponse))
                        if self.cMonTour : 
                            print ("c'est a moi de jouer")
                        else : 
                            print ("En attente de la case de l'autre")

                        self.etat_interface=2

                else:
                    self.active = False
                    self.active2 = False
                    
            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_BACKSPACE:
                    # Traitement lorsqu'une touche de retour arrière est pressée
                    if self.active_ip : 
                        self.text_IP = event.unicode[:-1]
                    else : 
                        self.text_PORT = event.unicode[:-1]
                else:
                    # Ajout du caractère à la chaîne de texte
                    if self.active_ip : 
                        self.text_IP += event.unicode
                    else :
                        self.text_PORT += event.unicode
            
    def attend_case(self):
        """
        
        PARAMETRES :
            aucun
        
        ACTION :
            Attends du serveur la case cliquée par l'autre joueur et l'ajoute en conséquent à une liste correspondante à sa situation (touché ou dans l'eau)
                                                                                                                                      
        """
        self.socket.sendto("case ?".encode(), (self.text_IP, int(self.text_PORT)));
        reponse = self.socket.recv(128)
        if len(reponse)>2 : 
            print(reponse.decode())
            case_recue = eval(reponse.decode())
            print(f"on a reçu {case_recue}")
            est_touche = self.grille.attaquer(case_recue)
            resultat_attaque =('entier', est_touche)
            print("self.gauche",self.gauche_liste_cases_eau)


            if est_touche==0:
                self.droite_liste_cases_eau.append(case_recue)
            if est_touche==1:
                self.droite_liste_cases_touchees.append(case_recue)
            if est_touche==2:
                self.droite_liste_cases_touchees.append(case_recue)


            print(f"On envoie le résultat de l'attaque : {resultat_attaque}")
            if self.grille.navires!=[]:
                self.socket.sendto(str.encode(str(resultat_attaque)), (self.text_IP, int(self.text_PORT)))
            else:
                self.socket.sendto("PERDU!".encode(), (self.text_IP, int(self.text_PORT)))
                self.etat_interface = 3
            resultat_attaque = self.socket.recv(128)   


            self.cMonTour=True
            self.case_cliquee=None

    def on_attend_reponse(self):
        """
        
        PARAMETRES :
            aucun
        
        ACTION :
            Attends du serveur la réponse (int) de la case envoyée à l'autre joueur
            
        """
        self.socket.sendto("entier ?".encode(), (self.text_IP, int(self.text_PORT)));
        resultat_attaque = self.socket.recv(128);
        
        if not "bien" in resultat_attaque.decode() and  len(resultat_attaque)>3 : 
            print("on a reçu", resultat_attaque.decode())

            message = eval(resultat_attaque.decode()) #un tuple
            result = str(message[1])
            if result == '0' :
                self.gauche_liste_cases_eau.append(self.case_cliquee)
            elif result == '1' :
                print('touché')
                self.gauche_liste_cases_touchees.append(self.case_cliquee)
            elif result == '2' :
                print('touché coulé')
                self.gauche_liste_cases_touchees.append(self.case_cliquee)
            else :  #on a gagné
                self.gauche_liste_cases_touchees.append(self.case_cliquee)
                self.etat_interface = 3
                print("stooooooooooooooooooooooooooooooooooooooooooooooooooooooooop")

                
            self.cMonTour = False
            self.case_cliquee = None
            self.on_attend_entier = False
        
    def on_loop(self):
        """
        
        PARAMETRES :
            aucun
        
        ACTION :
            Gère les tours, c'est à quel joueur de jouer ? 
        
        """
        if self.etat_interface==2 : 
                if self.cMonTour : 
                        if not (self.case_cliquee is None or self.case_cliquee==(None, None)): 
                            print("c'est mon tour")
                            message = ('case', self.case_cliquee)
                            print(f"On envoie au serveur {message}")
                            #self.case_cliquee= None
                            self.socket.sendto(str.encode(str(message)), (self.text_IP, int(self.text_PORT)))
                            reponse = self.socket.recv(1024);
                            print("reponse : ",reponse)
                            print("on attend l'entier")
                            self.on_attend_entier = True
                        if self.on_attend_entier:
                            self.on_attend_reponse()
                else : 
                    self.attend_case()
                
    def on_render(self):
        """
        
        PARAMETRES : 
            aucun
        
        ACTION :
            Gère l'intégralité de l'affichage du jeu (self.etat_interface = 2), de la partie grpahique. Cela va de la couleur du fond à l'affichage des bateaux jusqu'aux textes
        
        """
        
        pygame.display.set_caption("The Naval Strike Initiative - Jeu")
        BLEU_FONCE = (11,28,99)
        self.clock_move = pygame.time.get_ticks()-self.tick_depart
        self.clock_frame = pygame.time.get_ticks()-self.tick_depart
       
        if self.clock_move >= 10:
            self.tick_depart = pygame.time.get_ticks()
            for cercle in self.cercles:
                pygame.draw.ellipse(self._display_surf, (255,0,0), (cercle[0]+4-cercle[2], -cercle[2]+cercle[1]+4, cercle[2]*2, cercle[2]*2))
                cercle[2] +=1
                if cercle[2] == 12:
                    self.cercles.remove(cercle)
                    
        if self.clock_frame >= 11:
            self.tick_depart = pygame.time.get_ticks()
            self._display_surf.fill((57,92,152))
        
        #Affichage des bateaux
        for bateau in self.grille.navires:
                for i in range(len(bateau.positions)-1):
                    lettre, chiffre = bateau.positions[i]
                    if bateau.positions[-1]== "h" :#le bateau est horizontal
                            if i == 0:
                                self._display_surf.blit(self.pilImageToSurface(self.image_horiz_gauche), ((80+480+80+4+chiffre*48-48),(260+4+(ord(lettre)-65)*48),48,48))
                            elif i == len(bateau.positions)-2:
                                self._display_surf.blit(self.pilImageToSurface(self.image_horiz_droite), ((80+480+80+4+chiffre*48-48),(260+4+(ord(lettre)-65)*48),48,48))
                            else:
                                self._display_surf.blit(self.pilImageToSurface(self.image_horiz_milieu), ((80+480+80+4+chiffre*48-48),(260+4+(ord(lettre)-65)*48),48,48))
                    if bateau.positions[-1] == "v" :#le bateau est horizontal
                            if i == 0:
                                self._display_surf.blit(self.pilImageToSurface(self.image_vert_haut), ((80+480+80+4+chiffre*48-48),(260+4+(ord(lettre)-65)*48),48,48))
                            elif i == len(bateau.positions)-2:
                                self._display_surf.blit(self.pilImageToSurface(self.image_vert_bas), ((80+480+80+4+chiffre*48-48),(260+4+(ord(lettre)-65)*48),48,48))
                            else:
                                self._display_surf.blit(self.pilImageToSurface(self.image_vert_milieu), ((80+480+80+4+chiffre*48-48),(260+4+(ord(lettre)-65)*48),48,48))
        #
        
        #Affichage des textes
        text_surface = pygame.font.SysFont("Eight-Bit Madness", 100).render("THE", True, BLEU_FONCE)
        self._display_surf.blit(text_surface, (500, 40))
        
        text_surface = pygame.font.SysFont("Eight-Bit Madness", 100).render("NAVAL STRIKE INITIATIVE", True, BLEU_FONCE)
        self._display_surf.blit(text_surface, (140, 120))
        
        text_surface = pygame.font.SysFont("Eight-Bit Madness", 40).render("LUI", True, BLEU_FONCE)
        self._display_surf.blit(text_surface, (80+200, 260+480+20))
        
        text_surface = pygame.font.SysFont("Eight-Bit Madness", 40).render("MOI", True, BLEU_FONCE)
        self._display_surf.blit(text_surface, (80+480+80+200, 260+480+20))

        if self.cMonTour :
            text_surface = pygame.font.SysFont("Eight-Bit Madness", 40).render("Je choisis une case", True, BLEU_FONCE)
            self._display_surf.blit(text_surface, (80+400, 260+480+70))
        else:
            text_surface = pygame.font.SysFont("Eight-Bit Madness", 40).render("J'attends...", True, BLEU_FONCE)
            self._display_surf.blit(text_surface, (80+480+20, 260+480+70))
        #    
            
        pygame.draw.rect(self._display_surf, (11,28,99), (80,260,480,480), 2) #rectangle de la zone de mes bateaux
        pygame.draw.rect(self._display_surf, (11,28,99), (80+480+80,260,480,480), 2) #rectangle de la zone des bateaux de l'adversaire
        
        #Affichage des grilles
        for i in range(1,10):
            pygame.draw.rect(self._display_surf, (11,28,99), (80,260+i*48,480,1))
            pygame.draw.rect(self._display_surf, (11,28,99), (80+i*48,260,1,480))
            #grille de droite
            pygame.draw.rect(self._display_surf, (11,28,99), (80+480+80,260+i*48,480,1))
            pygame.draw.rect(self._display_surf, (11,28,99), ((80+480+80)+i*48,260,1,480))
        #
        
        #Affichage des croix et des ronds des cases à droite
        for case_touchee in self.droite_liste_cases_touchees: #si touchées
            if case_touchee is not None:
                pygame.draw.line(self._display_surf, (180,0,0), (80+480+80+5+case_touchee[1]*48-48,260+5+(ord(case_touchee[0])-65)*48), (80+480+80-5+case_touchee[1]*48,260-5+(ord(case_touchee[0])-65)*48+48), 4)
                pygame.draw.line(self._display_surf, (180,0,0), (80+480+80+48-5+case_touchee[1]*48-48,260+5+(ord(case_touchee[0])-65)*48), (80+480+80+5+case_touchee[1]*48-48,260-5+(ord(case_touchee[0])-65)*48+48), 4)
        for case_eau in self.droite_liste_cases_eau: #si dans l'eau
            if case_eau is not None:
                pygame.draw.ellipse(self._display_surf, (180,0,0), (80+480+80+5+case_eau[1]*48-48, 260+5+(ord(case_eau[0])-65)*48, 40, 40), 3)
        #
        
        #Affichage des croix et des ronds des cases à gauche
        for case_touchee in self.gauche_liste_cases_touchees:
            if case_touchee is not None:
                pygame.draw.line(self._display_surf, (0,180,0), (80+5+case_touchee[1]*48-48,260+5+(ord(case_touchee[0])-65)*48), (80-5+case_touchee[1]*48,260-5+(ord(case_touchee[0])-65)*48+48), 4)
                pygame.draw.line(self._display_surf, (0,180,0), (80+48-5+case_touchee[1]*48-48,260+5+(ord(case_touchee[0])-65)*48), (80+5+case_touchee[1]*48-48,260-5+(ord(case_touchee[0])-65)*48+48), 4)
        for case_eau in self.gauche_liste_cases_eau: #si dans l'eau
            if case_eau is not None:
                pygame.draw.ellipse(self._display_surf, (0,180,0), (80+5+case_eau[1]*48-48, 260+5+(ord(case_eau[0])-65)*48, 40, 40), 3)
        #
            
        #Affichages des lettres et des chiffres en colonnes et en lignes
        for i in range(10):
            #lettres
            text_surface = pygame.font.SysFont("Eight-Bit Madness", 40).render(chr(65+i), True, BLEU_FONCE)
            self._display_surf.blit(text_surface, (50, 275+48*i))
            self._display_surf.blit(text_surface, (80+480+50, 275+48*i))
            #chiffres
            text_surface = pygame.font.SysFont("Eight-Bit Madness", 40).render(f"{i+1}", True, BLEU_FONCE)
            self._display_surf.blit(text_surface, (80+480+80+15+48*i, 230))
            self._display_surf.blit(text_surface, (80+15+48*i, 230))
        #
        
            
        pygame.display.flip()
        
        
    def on_render_client(self):
        """
        
        PARAMETRES :
            aucun
        
        ACTION :
            Gère l'intégralité de l'affichage du début (self.etat_interface = 1), des inputs à la tortue
        
        """
        big_font = pygame.font.SysFont("Eight-Bit Madness", 64) 
        font = pygame.font.SysFont("Eight-Bit Madness", 32)  
        BLEU_FONCE = (11,28,99)
        BLEU_CLAIR = (57,92,152)
        color_inactive = pygame.Color(200,200,200)
        color_active = pygame.Color(11,28,99)  
        
        color = color_inactive  
        color2 = (168, 200, 50)
        c_ok = BLEU_FONCE
        YELLOW = 255,255,0
        
        pygame.display.set_caption("The Naval Strike Initiative - Début")
        
        # Texte pour les boîte de saisie 
        texte = font.render('SERVEUR', True, BLEU_FONCE)  
        texte2 = font.render('PORT', True, BLEU_FONCE) 
        
        color = color_active if self.active_port else color_inactive
        color2 = color_active if self.active_ip else color_inactive
        
        self._display_surf.fill(BLEU_CLAIR)
        txt_pos_x,txt_pos_y = 800,700
        self.pos_souris_x,self.pos_souris_y = pygame.mouse.get_pos()
        
        if txt_pos_x +65 >= self.pos_souris_x >= txt_pos_x and txt_pos_y + 37 >= self.pos_souris_y >= txt_pos_y :
            c_ok = YELLOW

        text_surface = big_font.render("OK", True, c_ok)
        
        text_surface_t = pygame.font.SysFont("Eight-Bit Madness", 100).render("THE", True, BLEU_FONCE)
        self._display_surf.blit(text_surface_t, (500, 40))
        
        text_surface_T = pygame.font.SysFont("Eight-Bit Madness", 100).render("NAVAL STRIKE INITIATIVE", True, BLEU_FONCE)
        self._display_surf.blit(text_surface_T, (140, 120))
        
        txt_surface = font.render(self.text_IP, True, BLEU_FONCE)
        txt_surface_error = font.render(self.text_Error, True, BLEU_FONCE)
        width = max(200, txt_surface.get_width()+10)
        self.input_box.w = width
        
        self._display_surf.blit(txt_surface, (self.input_box.x+5, self.input_box.y+5))
        self._display_surf.blit(txt_surface_error, (self.input_box.x-50, self.input_box.y+50))
        
        self._display_surf.blit(texte, (self.input_box.x, self.input_box.y-30))
        
        pygame.draw.rect(self._display_surf, color, self.input_box, 2)
        
        
        self._display_surf.blit(texte2, (self.input_box2.x, self.input_box2.y-30))
        txt_surface2 = font.render(self.text_PORT, True, BLEU_FONCE)
        width2 = max(200, txt_surface2.get_width()+10)
        self.input_box2.w = width2
        self._display_surf.blit(txt_surface2, (self.input_box2.x+5, self.input_box2.y+5))
        pygame.draw.rect(self._display_surf, color2, self.input_box2, 2)
 
        self._display_surf.blit(self.pilImageToSurface(self.image_tortue), (400,400,40,40))
        self._display_surf.blit(self.pilImageToSurface(self.image_horiz_gauche), (800,800,40,40))
        self._display_surf.blit(self.pilImageToSurface(self.image_horiz_droite), (926,800,40,40))
        self._display_surf.blit(self.pilImageToSurface(self.image_horiz_milieu), (842,800,40,40))
        self._display_surf.blit(self.pilImageToSurface(self.image_horiz_milieu), (884,800,40,40))        
        
        
        pygame.draw.rect(self._display_surf, BLEU_CLAIR , self.button_ok_connect)
        self._display_surf.blit(text_surface, (800,700))
        
        pygame.display.flip()
        
        pygame.display.flip()
        
    def on_render_fin(self):
        """
        
        PARAMETRES :
            aucun
        
        ACTION :
            Gère l'intégralité de l'affichage de la fin (self.etat_interface = 3), les textes
        
        """
        
        pygame.display.set_caption("The Naval Strike Initiative - Fin")
        BLEU_FONCE = (11,28,99)
        BLEU_CLAIR = (57,92,152)
        
        self._display_surf.fill(BLEU_CLAIR)
        
        text_surface_t = pygame.font.SysFont("Eight-Bit Madness", 100).render("THE", True, BLEU_FONCE)
        self._display_surf.blit(text_surface_t, (500, 40))
        
        text_surface_T = pygame.font.SysFont("Eight-Bit Madness", 100).render("NAVAL STRIKE INITIATIVE", True, BLEU_FONCE)
        self._display_surf.blit(text_surface_T, (140, 120))
        
        if self.grille.navires == []:
            text_gagne = pygame.font.SysFont("Eight-Bit Madness", 100).render("PERDU ...", True, (206,212,7))
        else :
            text_gagne = pygame.font.SysFont("Eight-Bit Madness", 100).render("GAGNE !!!", True, (206,212,7))
        self._display_surf.blit(text_gagne, (80+360, 260+200))
        
        pygame.display.flip()
        
    def on_cleanup(self):
        """
        
        PARAMETRES:
            aucun
            
        ACTION :
            Nettoie l'affichage pour en afficher un nouveau
            
        """
        pygame.quit()
 
    def on_execute(self):
        """
        
        PARAMETRES :
            aucun
            
        ACTION :
            Gère le déroulement du jeu
        
        """
        if self.on_init() == False:
            self._running = False
        
        if self.etat_interface==0 : #interface réseau
            pygame.display.set_caption("Menu des joueurs")    
        
        while self._running :
            for event in pygame.event.get():
                self.on_event(event)
                
                
            if self.etat_interface==2 : 
                self.on_render()
                self.on_loop()
                
            if self.etat_interface==3 : 
                self.on_render_fin()                   

            
            elif self.etat_interface==1 :     
                self.on_render_client()
        self.on_cleanup()
    

if __name__ == "__main__" :
    theApp = App()
    theApp.on_execute()
